-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 07-Mar-2019 às 20:59
-- Versão do servidor: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salon`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agenda`
--

DROP TABLE IF EXISTS `agenda`;
CREATE TABLE IF NOT EXISTS `agenda` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Cliente` varchar(500) DEFAULT NULL,
  `Horario` varchar(500) DEFAULT NULL,
  `Data` varchar(500) DEFAULT NULL,
  `Procedimento` varchar(500) DEFAULT NULL,
  `Formapg` varchar(500) DEFAULT NULL,
  `Receber` varchar(500) DEFAULT NULL,
  `Telefone` varchar(500) DEFAULT NULL,
  `Status` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `agenda`
--

INSERT INTO `agenda` (`Id`, `Cliente`, `Horario`, `Data`, `Procedimento`, `Formapg`, `Receber`, `Telefone`, `Status`) VALUES
(1, '1', '15:00', '2019-02-26', '', '', '20,00', '(81) 9999-9999', 'Ativo'),
(2, 'Filipe', '15:00', '2019-02-26', '', '', '20,00', '(81) 9999-9999', 'Ativo'),
(3, 'Filipe', '12:00', '2019-02-26', 'uu', 'uu', '20,00', '(81) 9999-9999', 'Ativo'),
(4, 'Filipe', '15:00', '2019-02-26', 'Progressiva', 'uu', '20,00', '(81) 9999-9999', 'Ativo'),
(5, 'Filipe', '15:00', '2019-02-26', 'Progressiva', 'CartÃ£o de CrÃ©dito', '20,00', '(81) 9999-9999', 'Ativo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(500) DEFAULT NULL,
  `CPF` varchar(500) DEFAULT NULL,
  `RG` varchar(500) DEFAULT NULL,
  `Rua` varchar(500) DEFAULT NULL,
  `Bairro` varchar(500) DEFAULT NULL,
  `CEP` varchar(500) DEFAULT NULL,
  `Numero` varchar(500) DEFAULT NULL,
  `Telefone` varchar(500) DEFAULT NULL,
  `Status` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`Id`, `Nome`, `CPF`, `RG`, `Rua`, `Bairro`, `CEP`, `Numero`, `Telefone`, `Status`) VALUES
(1, 'Amanda', '3333333333', '88888', 'euclides', 'ddd', '55030410', '33', '(81) 9999-9999', NULL),
(2, 'Filipe', '3333333333', '88888', 'euclides', 'ddd', '55030410', '33', '(81) 9999-9999', 'Ativo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `financeiro`
--

DROP TABLE IF EXISTS `financeiro`;
CREATE TABLE IF NOT EXISTS `financeiro` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Valor` varchar(500) NOT NULL,
  `Cliente` varchar(500) NOT NULL,
  `Data` varchar(500) NOT NULL,
  `Observacao` varchar(500) NOT NULL,
  `Recebido` varchar(500) NOT NULL,
  `Id_agenda` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id_agenda` (`Id_agenda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `financeiro`
--
ALTER TABLE `financeiro`
  ADD CONSTRAINT `financeiro_ibfk_1` FOREIGN KEY (`Id_agenda`) REFERENCES `agenda` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
