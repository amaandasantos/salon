<?php

require_once 'crud/crud_agenda.php';
require_once 'bancodedados/conexao.php';

$resultado_agenda =  "SELECT * FROM agenda WHERE Status = 'Ativo'";
$resultados =  mysqli_query($conn, $resultado_agenda);


//ESTANCIANDO A CLASSES
$objFunc = new agenda();

if(isset($_POST['btn_modal'])){
  $objFunc->update($_POST);
    header('location: listaagenda.php');
 }


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/escolhas.css">
     <link rel="stylesheet" href="css/clientes.css">
     <link rel="stylesheet" type="text/css" href="css/universal.css">
     <link rel="stylesheet" type="text/css" href="css/agenda.css">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
     <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
     <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
     <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Clientes</title>
  </head>
  <body>
 <nav class="navbar navbar-expand-lg navbar-light" id="nav">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav" id="l">
      <li class="nav-item active" id="l">
        <a class="nav-link" href="#" id="lc"><strong>Voltar</strong></a>
      </li>

    </ul>
  </div>
</nav>
<br>
  <div class="p" id="p" style="text-align: center; font-size: 20px;">
     <p id="p"> <strong>Agenda</strong> </p>
   </div>

   <hr>

   <div class="container">

<hr>

    <div class="agenda">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 15px;">Dia</th>
                        <th style="font-size: 15px;">Hora</th>
                        <th style="font-size: 15px;">Procedimento</th>
                        <th style="font-size: 15px;">Cliente</th>
                    </tr>
                </thead>
                <tbody >
                    <?php while ($row = mysqli_fetch_assoc($resultados)) { ?>
                    <!-- Single event in a single day -->
                    <tr>
                        <td class="agenda-date" class="active" rowspan="1" style="font-size: 15px;">
                            <?php echo $row['Data'];?></div>

                        </td>
                        <td class="agenda-time" style="font-size: 15px;">
                           <?php echo $row['Horario'];?>
                        </td>
                        <td class="agenda-events" style="font-size: 15px;">
                            <div class="agenda-event">
                                 <?php echo $row['Procedimento'];?>
                            </div>
                        </td>
                         <td class="agenda-events" style="font-size: 15px;">
                            <div class="agenda-event">
                                 <?php echo $row['Cliente'];?>
                            </div>
                        </td>



                        <td class="actions" style="font-size: 28px;">
                          <a data-toggle="modal" data-target="#modal" class="btn btn-success" onclick="setaDadosModal('<?php echo $row["Id"]; ?>')">
    <span class="btn-label"><i class="fa fa-check"></i></span>
</a>

                          <!--<button type="button" class="btn btn-warning" alt="Finalizado"><i class="fas fa-ban"></i></i></button>
                          <button type="button" class="btn btn-danger" alt="Finalizado"><i class="fas fa-trash-alt"></i></button> -->
                          <button type="button" class="btn btn-secondary" alt="Finalizado"><i class="fas fa-plus-circle"></i></button>
                         <a class="btn btn-info" href=''><i class="fas fa-user-edit"></i></a><br>

          </td>
                    </tr>
                    <?php } ?>
                    <!-- Multiple events in a single day (note the rowspan) -->

                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
function setaDadosModal(valor) {
    document.getElementById('id').value = valor;
}
</script>

<div id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Resumo do atendimento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form id="modalExemplo" method="POST" action="">
                        <input type="text" name="id" id="id" style="display: none;">

                        <div class="form-group col-md-12">
           <label for="inputCPF">Status do atendimento</label>
            <select class="form-control" name="status">
              <option value=""></option>
              <option>Finalizado</option>
              <option>Cancelado</option>
              <option>Remarcado</option>
            </select>
          </div>
          <div class="form-group col-md-12">
           <label for="inputCPF">Pagamento</label>
            <select class="form-control" name="procedimento">
              <option value=""></option>
              <option>Recebido</option>
              <option>Não recebido</option>
            </select>
          </div>
           <div class="form-group col-md-4">
           <label for="inputValor">Valor recebido:</label>
            <input type="text" class="form-control" id="valor" placeholder="EX.: R$ 150,00" name="valor" >
          </div>
            <div class="form-group col-md-4">
           <label for="inputValor">Data Recebimento:</label>
            <input type="date" class="form-control" id="data" name="data" >
          </div>
          <div class="form-group col-md-4">
           <label for="inputValor">Pago por:</label>
            <input type="text" class="form-control" id="nome" name="nome" >
          </div>
          <div class="form-group col-md-12">
           <label for="inputValor">Observação:</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
          </div>
<div class="col-lg-12" align="center" >

            <button type="submit" class="btn btn-info" id="btn_modal" name="btn_modal">Salvar</button>
         <button type="submit" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
   </div>
        </form>


      </div>

                </div>
            </div>
        </div>
    </div>
</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>