<?php

require_once 'bancodedados/conexao.php';
require_once 'crud/crud_agenda.php';

$objFunc = new agenda();

if(isset($_POST['btn_cadastrar'])){
  $objFunc->Insert($_POST);
  header('location: agenda.php');
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/escolhas.css">
     <link rel="stylesheet" href="css/clientes.css">
     <link rel="stylesheet" type="text/css" href="css/universal.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Clientes</title>
  </head>
  <body>
 <nav class="navbar navbar-expand-lg navbar-light" id="nav">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav" id="l">
      <li class="nav-item active" id="l">
        <a class="nav-link" href="#" id="lc"><strong>Voltar</strong></a>
      </li>

    </ul>
  </div>
</nav>
<br>
 <div class="p" id="p" style="text-align: center; font-size: 20px;">
     <p id="p"> <strong>Agende um horário para o seu cliente!</strong> </p>
   </div>

   <hr>
  <div class="col pt-lg-1 pb-2" id="divexterna">
    <div class="col-lg-6 col-sm-12 mt-5 col-md-6 p-0 " id="divinterna" >
   <form method="post">

  <div class="form-group">
   <div class="form-row mt-3 ">
          <div class="form-group col-md-4">
            <label for="inputNomeCompleto">Cliente</label>
           <select class="form-control" name="cliente">
           <option value=""></option>
       <?php
       $resultado_cliente = "SELECT * FROM cliente";
       $result = mysqli_query($conn, $resultado_cliente);
        while($row = mysqli_fetch_assoc($result)) { ?>
         <option value="<?php echo $row['Nome']; ?>"><?php echo $row['Nome']; ?></option> <?php

      }

       ?>
            </select>
          </div>
          <div class="form-group col-md-4">
           <label for="inputCPF">Horário</label>
            <input type="text" class="form-control" id="hr" placeholder="Ex.: 12:00" name="hr">
          </div>

          <div class="form-group col-md-4">
           <label for="inputCPF">Data</label>
            <input type="date" class="form-control" id="data" placeholder="Data" name="data">
          </div>
</div>

 <div class="form-row mt-3 ">
     <div class="form-group col-md-4">
           <label for="inputCPF">Procedimento</label>
            <select class="form-control" name="procedimento">
              <option value=""></option>
              <option>Progressiva</option>
              <option>Corte</option>
              <option>Pitura</option>
              <option>Luzes</option>
              <option>Hidratação</option>
              <option>Botox</option>
              <option>Alisamento</option>
              <option>Inteligente</option>

            </select>
          </div>
          <div class="form-group col-md-4">
            <label for="inputNomeCompleto">Forma de Pagamento</label>
            <select class="form-control" name="fp">
              <option>Dinheiro</option>
              <option>Cartão de Crédito</option>
              <option>Cartão de Débito</option>
              <option>Outro</option>
            </select>
          </div>
          <div class="form-group col-md-4">
           <label for="inputCPF">À receber</label>
            <input type="text" class="form-control" id="valor" placeholder="Ex.: R$: 150,00" name="valor">
          </div>


</div>

 <div class="form-row mt-3 ">

          <div class="form-group col-md-4">
           <label for="inputCPF">Telefone Cliente</label>
            <input type="text" class="form-control" id="telefone" placeholder="Telefone" name="telefone">
          </div>

          <div class="form-group col-md-4">
           <label for="inputCPF">Status:</label>
            <input type="text" class="form-control" id="status" placeholder="Ativo" name="status" value="ATIVO" disabled>
          </div>
</div>
<br>
<div class="col-lg-12" align="center" >

          <button type="submit" class="btn btn-light" name="btn_cadastrar" id="btn_cadastrar" style="background-color: #DC8CF2; color: white;">Cadastrar</button>
          <a href="escolha.php"><button type="button" class="btn btn-light" style="background-color: #F28CCB; color: white;">Cancelar</button></a>
   </div>

</div>
</div>
</div>
</form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>