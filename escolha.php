<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <link rel="stylesheet" href="index.css">
     <link rel="stylesheet" href="css/escolhas.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="stylesheet" href="css/escolhas.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light" id="nav">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav" id="l">
      <li class="nav-item active" id="l">
        <a class="nav-link" href="#" id="l"><strong>Fidelidade</strong></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="l"><strong>Financeiro</strong></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="l"><strong>Pacotes</strong></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="l"><strong>Comissão</strong></a>
      </li>
    </ul>
  </div>
</nav>
 <div class="jumbotron jumbotron-fluid" id="jb" >
  <div class="container">
    <div class="card-deck">
  <div class="card">
    <img class="card-img-top" src="img/cliente.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Cadastrar Clientes</h5>
      <p class="card-text">Cadastre todos os seus clientes!</p>
      <a href="clientes.php" class="btn btn-primary btn-sm" id="button" role="button" aria-pressed="true">Cadastrar</a>

    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="img/agendaa.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Agenda</h5>
      <p class="card-text">Marque o horário das suas clientes!</p>
      <a href="listaagenda.php" class="btn btn-primary btn-sm" id="button" role="button" aria-pressed="true">Agendar</a>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="img/funcionario.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Funcionários</h5>
      <p class="card-text">Gerencie os seus funcionários!</p>
      <a href="#" class="btn btn-primary btn-sm" id="button" role="button" aria-pressed="true">Gerenciar</a>
    </div>
  </div>
</div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>